<h3 align="center">ReportPro数据报表 云端版</h3>

<h4 align="center">小程序、H5、PC等多端兼容</h4>

<div align="center">

[![star](https://gitee.com/ureport/ReportPro/badge/star.svg)](https://gitee.com/ureport/ReportPro.git)  [![fork](https://gitee.com/ureport/ReportPro/badge/fork.svg)](https://gitee.com/ureport/ReportPro.git)  [![](https://img.shields.io/badge/插件市场-ReportPro-red)](https://ext.dcloud.net.cn/plugin?id=4651)  [![](https://img.shields.io/badge/QQ群-878946748-red)](https://qm.qq.com/cgi-bin/qm/qr?k=ar_gonKNpbGHOiAxKXuE-9VRY89n-3kT&jump_from=webapi)



```shell
无偿开源！你们的Star是我的动力！
```

------------------------------------------------------------------------


</div>

# ReportPro 云端数据报表中心

## 该小程序基于uniCloudAdmin框架开发，主要使用了ucharts和wyb-table两插件实现的数据报表功能，感谢作者秋云和SirW的优秀开源项目

## 下载地址
- [码云Gitee开源](https://gitee.com/ureport/ReportPro)
- [Dcloud插件市场](https://ext.dcloud.net.cn/plugin?id=4651)

## 特点
- 前身为[UReport插件](https://ext.dcloud.net.cn/plugin?id=4373)，与`UReport`最大不同的：此插件是`UReport`的升级版，采取了[uniCloud](https://uniapp.dcloud.io/uniCloud/README)云函数开发实现数据中心报表功能。

- 采用[admin](https://uniapp.dcloud.io/uniCloud/admin?id=admin-%E6%8F%92%E4%BB%B6%E5%BC%80%E5%8F%91)插件开发，管理员可配置不同用户的权限来浏览对应的页面，自由实现数据的增删改查

- 是一款真正的[uniCloud Admin](https://uniapp.dcloud.io/uniCloud/admin?id=unicloud-admin-%E6%A1%86%E6%9E%B6) 数据报表插件

- 结合了最新的`qiun-data-charts`插件完成开发，其作者是开发ucharts的大神秋云，他一直致力于精美、灵活的图表插件开发，故无须担心图表方面的问题

## ReportPro各端展示
- 小程序
- ![小程序](https://howcode.online/photo/project/20210406153611149.gif#pic_center)

- H5
- ![H5](https://howcode.online/photo/project/20210406153633832.gif#pic_center)


- PC端
- ![PC](https://howcode.online/photo/project/20210406153848741.gif#pic_center)



## 使用说明
- 到[uniCloud控制台](https://unicloud.dcloud.net.cn)下创建一个属于自己的服务空间。注意：该步骤需要实名认证，并且确保账号与HBuilder登陆的账号一致，否则项目关联不上云服务空间

- 回到项目中选中uniCloud云空间，右键选中关联云服务空间，选择关联之前创建的服务空间

- 展开uniCloud文件，选中database目录下的db_init.json文件，右键选择`初始化云数据库`

- 选中cloudfunctions文件，右键选择`上传所有云函数、公共模块及action`

- 到此项目即可运行起来，如果想将图表放到自己的项目中运行，可查看[how-code插件](https://gitee.com/howcode/ReportPro/tree/master/uni_modules/howcode-report)说明文档

## 注意事项
- 项目运行起来时，打开微信开发者工具->右上角详情->本地设置->调试基础库，选择最新版本2.16.0或以上
- 发布到小程序时，在微信公众号平台的开发管理中，配置request合法域名：[https://api.bspapp.com];[https://wthrcdn.etouch.cn];此两项为uniCloud云函数的发起接口api

## 云端表格
### 用户
#### 用户表
表名：uni-id-users

| 字段						| 类型			| 必填| 描述																											|
| ----------------| ---------	| ----| -------------------------------------------								|
| \_id						| Object ID	| 是	| 存储文档 ID（用户 ID），系统自动生成											|
| username				| String		| 否	| 用户名，不允许重复																				|
| password				| String		| 否	| 密码，加密存储																						|
| nickname				| String		| 否	| 用户昵称																									|
| gender					| Integer		| 否	| 用户性别：0 未知 1 男性 2 女性														|
| status					| Integer		| 是	| 用户状态：0 正常 1 禁用 2 审核中 3 审核拒绝								|
| mobile					| String		| 否	| 手机号码																									|
| mobile_confirmed| Integer		| 否	| 手机号验证状态：0 未验证 1 已验证，未验证用户不可登录			|
| email						| String		| 否	| 邮箱地址																									|
| email_confirmed	| Integer		| 否	| 邮箱验证状态：0 未验证 1 已验证，未验证用户不可登录				|
| avatar					| String		| 否	| 头像地址																									|
| wx_unionid			| String		| 否	| 微信unionid																								|
| wx_openid				| Object		| 否	| 微信各个平台openid																				|
| ali_openid			| String		| 否	| 支付宝平台openid																					|
| comment					| String		| 否	| 备注																											|
| realname_auth		| Object		| 否	| 实名认证信息																							|
| register_date		| Timestamp	| 否	| 注册时间																									|
| register_ip			| String		| 否	| 注册时 IP 地址																						|
| last_login_date	| Timestamp	| 否	| 最后登录时间																							|
| last_login_ip		| String		| 否	| 最后登录时 IP 地址																				|
| login_ip_limit	| Array			| 否	| 登录 IP 限制																							|
| inviter_uid			| Array			| 否	| 邀请人uid，按层级从下往上排列的uid数组，即第一个是直接上级|
| my_invite_code	| String		| 否	| 用户自己的邀请码																					|
| role						| Array			| 否	| 用户角色列表，由role_id组成的数组			

#### 验证码表

表名：`uni-verify`

| 字段       | 类型      | 必填 | 描述                                   |
| ---------- | --------- | ---- | -------------------------------------- |
| \_id       | Object ID | 是   | 存储文档 ID（验证码 ID），系统自动生成 |
| mobile     | String    | 是   | 手机号，和邮箱二选一                 |
| email      | String    | 是   | 邮箱，和手机号二选一                  |
| code       | String    | 是   | 验证码                                 |
| type       | String   | 是   | 验证类型：login, bind, unbind, pay     |
| state      | Integer   | 是   | 验证状态：0 未验证 1 已验证 2 已作废    |
| ip         | String    | 是   | 请求时 IP 地址                         |
| created_at | Timestamp | 是   | 创建时间                               |
| expired_at | Timestamp | 是   | 验证码过期时间                         |

#### 角色表

表名：`uni-id-roles`

| 字段				| 类型			| 必填| 描述																	|
| ----------	| ---------	| ----| --------------------------------------|
| \_id				| Object ID	| 是	| 系统自动生成的Id											|
| role_id			| String		| 是	| 角色唯一标识													|
| role_name		| String		| 否	| 角色名，展示用												|
| permission	| Array			| 是	| 角色拥有的权限列表										|
| comment			| String		| 否	| 备注																	|
| created_date| Timestamp	| 是	| 角色创建时间													|

#### 权限表

表名：`uni-id-permissions`

| 字段						| 类型			| 必填| 描述																	|
| ----------			| ---------	| ----| --------------------------------------|
| \_id						| Object ID	| 是	| 系统自动生成的Id											|
| permission_id		| String		| 是	| 权限唯一标识													|
| permission_name	| String		| 否	| 权限名，展示用												|
| comment					| String		| 否	| 备注																	|
| created_date		| Timestamp	| 是	| 权限创建时间													|

#### 日志表

表名：`uni-id-log`

| 字段						| 类型			| 必填| 描述																	|
| ----------			| ---------	| ----| --------------------------------------|
| \_id						| Object ID	| 是	| 用户id，参考uni-id-users表											|
| user_id| String		| 是	| 权限唯一标识													|
| ua| String		| 否	| userAgent											|
| device_uuid| String		| 否	| 设备唯一标识(需要加密存储)|
| type| String		| 否	| 登录类型：login代表登陆，logout代表退出|
| state| String		| 否	| 操作状态：结果0 失败、1 成功|
| ip| String		| 否	| ip地址|
| created_date		| Timestamp	| 是	| 权限创建时间													|

具体看[Dcloud的uni-id设计](https://gitee.com/dcloud/uni-id#%E6%95%B0%E6%8D%AE%E5%BA%93%E7%BB%93%E6%9E%84)

### 文章&评论

#### 文章表

表名：opendb-news-articles

| 字段								| 类型			| 必填| 描述																				|
| ----------------		| ---------	| ----| -------------------------------------------	|
| \_id								| Object ID	| 是	| 存储文档 ID（文章 ID），系统自动生成				|
| user_id							| String		| 是	| 文章作者ID， 参考`uni-id-users` 表					|
| category_id					| String		| 否	| 分类 id，参考`opendb-news-categories`表				|
| title								| String		| 是	| 标题																				|
| content							| String		| 是	| 文章内容																		|
| excerpt							| String		| 否	| 文章摘录																		|
| article_status			| Integer		| 是	| 文章状态：0 草稿箱 1 已发布									|
| view_count					| Integer		| 是	| 阅读数量																		|
| like_count					| Integer		| 是	| 喜欢数、点赞数															|
| is_sticky						| Boolean		| 是	| 是否置顶																		|
| is_essence					| Boolean		| 是	| 阅读加精																		|
| comment_status			| Integer		| 是	| 评论状态：0 关闭  1 开放										|
| comment_count				| Integer		| 是	| 评论数量																		|
| last_comment_user_id| String		| 否	| 最后回复用户 id，参考`uni-id-users` 表			|
| avatar							| String		| 否	| 缩略图地址																	|
| publish_date				| Timestamp	| 否	| 发表时间																		|
| publish_ip					| String		| 否	| 发表时 IP 地址															|
| last_modify_date		| Timestamp	| 否	| 最后修改时间																|
| last_modify_ip			| String		| 否	| 最后修改时 IP 地址													|

#### 文章类别

表名：opendb-news-categories

| 字段						| 类型			| 必填| 描述																				|
| ----------------| ---------	| ----| -------------------------------------------	|
| \_id						| Object ID	| 是	| 存储文档 ID（文章 ID），系统自动生成				|
| name						| String		| 是	| 类别名称																		|
| description			| String		| 是	| 类别描述																		|
| icon						| String		| 是	| 类别图标地址																|
| sort						| Integer		| 否	| 类别显示顺序																|
| article_count		| Integer		| 否	| 该类别下文章数量														|
| create_date			| Timestamp	| 否	| 创建时间																		|


#### 评论表

表名：opendb-news-comments

| 字段						| 类型			| 必填| 描述																					|
| ----------------| ---------	| ----| -------------------------------------------		|
| \_id						| Object ID	| 是	| 存储文档 ID（文章 ID），系统自动生成					|
| article_id			| String		| 是	| 文章ID，opendb-news-posts 表中的`_id`字段				|
| user_id					| String		| 是	| 评论者ID，参考`uni-id-users` 表								|
| comment_content	| String		| 是	| 评论内容																			|
| like_count			| Integer		| 是	| 评论喜欢数、点赞数														|
| comment_type		| Integer		| 是	| 回复类型： 0 针对文章的回复  1 针对评论的回复	|
| reply_user_id		| String		| 是	| 被回复的评论用户ID，comment_type为1时有效			|
| reply_comment_id| String		| 是	| 被回复的评论ID，comment_type为1时有效					|
| comment_date		| Timestamp	| 否	| 评论发表时间																	|
| comment_ip			| String		| 否	| 评论发表时 IP 地址														|

#### 收藏表

表名：opendb-news-favorite

| 字段						| 类型			| 必填| 描述																					|
| ----------------| ---------	| ----| -------------------------------------------		|
| \_id						| Object ID	| 是	| 存储文档 ID（文章 ID），系统自动生成					|
| article_id 	| String		| 是	| 文章ID，opendb-news-posts 表中的`_id`字段				|
| user_id					| String		| 是	| 评论者ID，参考`uni-id-users` 表								|
| create_date			| Timestamp	| 否	| 创建时间																		|

具体查看[opendb-news设计](https://gitee.com/dcloud/opendb/blob/master/opendb-news.md#)

### 电商系统

#### 商品表


表名：opendb-mall-goods

| 字段								| 类型			| 必填| 描述																																|
| ----------------		| ---------	| ----| -------------------------------------------													|
| \_id								| Object ID	| 是	| 存储文档 ID（商品 ID），系统自动生成																|
| category_id					| String		| 否	| 分类 id，参考`opendb-mall-categories`表																|
| goods_sn						| String		| 是	| 商品的唯一货号																											|
| name								| String		| 是	| 商品名称																														|
| keywords						| String		| 否	| 商品关键字，为搜索引擎收录使用																			|
| goods_desc					| String		| 否	| 商品详细描述																												|
| goods_thumb					| String		| 否	| 商品缩略图，用于在列表或搜索结果中预览显示													|
| goods_banner_imgs		| Array			| 否	| 商品详情页的banner图地址																						|
| remain_count				| int		| 是	| 库存数量																														|
| month_sell_count		| int		| 是	| 月销量																															|
| total_sell_count		| int		| 是	| 总销量																															|
| comment_count				| int		| 是	| 累计评论数																													|
| is_real							| Boolean		| 是	| 是否实物																														|
| is_on_sale					| Boolean		| 是	| 是否上架销售																												|
| is_alone_sale				| Boolean		| 是	| 是否能单独销售；如果不能单独销售，则只能作为某商品的配件或者赠品销售|
| is_best							| Boolean		| 是	| 是否精品																														|
| is_new							| Boolean		| 是	| 是否新品																														|
| is_hot							| Boolean		| 是	| 是否热销																														|
| add_date						| Timestamp	| 否	| 上架时间																														|
| last_modify_date		| Timestamp	| 否	| 最后修改时间																												|
| seller_note					| String		| 否	| 商家备注，仅商家可见																												|

#### 商品 SKU 表

表名：opendb-mall-sku

名词解释：sku：Stock Keeping Unit。

sku 在电商系统中可定义库存控制的最小单元，同一个商品如有多个属性，则可定义多个sku。举例来说，某手机有8G内存和16G内存，则可定义两个sku，表示这种型号的差异。

理论上，基于商品的每个属性差异，可定义多个sku，比如颜色、重量、大小、材质等，故opendb中的商品sku，仅定义通用售价、库存数量等参数，各属性差异可由开发者自行扩展。

| 字段				| 类型		| 必填	| 描述											|
| ----------------	| ---------	| ----	| -------------------------------------------	|
| \_id				| Object ID	| 是	| 存储文档 ID（sku ID），系统自动生成			|
| goods_id			| String	| 是	| 商品 id，参考 opendb-mall-goods 表			|
| sku_name			| String	| 是	| SKU名称										|
| price				| int		| 是	| 价格，以分为单位，避免浮点计算的精度问题		|
| market_price		| int		| 否	| 市场价，以分为单位，避免浮点计算的精度问题	|
| stock				| int		| 是	| 库存数量										|
| create_date		| Timestamp	| 是	| 创建时间										|
| update_date		| Timestamp	| 否	| 修改时间										|

#### 商品类别

表名：opendb-mall-categories

| 字段						| 类型			| 必填| 描述																				|
| ----------------| ---------	| ----| -------------------------------------------	|
| \_id						| Object ID	| 是	| 存储文档 ID（商品类别 ID），系统自动生成				|
| name						| String		| 是	| 类别名称																		|
| description			| String		| 是	| 类别描述																		|
| create_date			| Timestamp	| 否	| 创建时间																		|


#### 商品评论表

表名：opendb-mall-comments

| 字段						| 类型			| 必填| 描述																				|
| ----------------| ---------	| ----| -------------------------------------------	|
| \_id						| Object ID	| 是	| 存储文档 ID（评论 ID），系统自动生成				|
| goods_id				| String		| 是	| 商品ID，opendb-mall-goods 表中的`_id`字段			|
| user_id					| String		| 是	| 评论者ID，参考`uni-id-users` 表							|
| comment_content	| String		| 是	| 评论内容																		|
| comment_date		| Timestamp	| 否	| 评论发表时间																|
| comment_ip			| String		| 否	| 评论发表时 IP 地址													|

#### 订单表

表名：opendb-mall-order

| 字段						| 类型			| 必填| 描述																				|
| ----------------| ---------	| ----| -------------------------------------------	|
| \_id						| Object ID	| 是	| 存储文档 ID（订单 ID），系统自动生成				|
| order_guid			| String		| 是	| 唯一订单编号			|
| user_id					| String		| 是	| 下单用户ID， 参考`uni-id-users` 表							|
| good_id					| String		| 是	| 商品id，参考`uni-mall-goods`表								|
| platform_type		| int| 是	| 平台类型：1为线下，2为O2O，3为B2C									|
| total_cash			| decimal| 是	| 实付金额													|
| discount_cash| decimal| 否	| 优惠金额													|
| is_promotion| int| 是	| 提取状态：0为待提取，1为提取									|
| payment_date	| Timestamp	| 否	| 支付时间																		|
| cancel_date			| Timestamp	| 否	| 取消时间																		|
| create_date			| Timestamp	| 否	| 创建时间																		|

#### 指标表

表名：opendb-mall-norm

| 字段						| 类型			| 必填| 描述																				|
| ----------------| ---------	| ----| -------------------------------------------	|
| \_id						| Object ID	| 是	| 存储文档 ID（指标 ID），系统自动生成				|
| name| String		| 是	| 指标名称			|
| expect| int| 是	| 目标值							|
| average| String		| 是	| 平均完成水平								|

具体查看[opendb-mall设计](https://gitee.com/dcloud/opendb/blob/master/opendb-mall.md#)

## 常见问题
### 问题一：图表偶尔加载不出或直接显示报错页面
- 原因：canvasId重复导致图表加载不出；canvasId丢失导致无法获得dom；
- 解决方法：请务必在组件上定义canvasId，不能为纯数字、不能为变量、不能重复、尽量长一些;请检查微信小程序的基础库，修改至2.16.0或者最新版本的基础库;请检查父元素或父组件是否用v-if来控制显示，如有请改为v-show，并将v-show的逻辑绑定至组件。

### 问题二：图表点击显示不出提示信息或点击位置不精准
- 原因：组件内嵌套组件 + 开启2d模式 + 组件中使用scroll-view标签，使得获取图表的定位不准
- 解决方法：组价内加入属性 :inScrollView="true" :pageScrollTop="pageScrollTop" :ontouch="true" ，其中pageScrollTop为当前滚动距离顶部的高度

### 更多问题查看：[秋云图表组件工具](https://demo.ucharts.cn/#/) -> 常见问题

## 后期计划

- 新增更多报表页面
- 探索qiun-data-charts下datacom的高阶用法

## 使用手册
[uchart官网](https://demo.ucharts.cn/#/)
[table插件使用说明](https://ext.dcloud.net.cn/plugin?id=2667)

## 交流群
- 微信群，添加`howcoder`微信拉群（关于插件解答、技术、行业、兴趣交流）

## 😊 捐助作者

<table>
	<tr>
		<td><img src="https://howcode.online/photo/project/pSeE0FH.jpg" width="220"/></td>
		<td><img src="https://howcode.online/photo/project/pSeEylt.jpg" width="220" /></td>
	</tr>
</table>
<code>👍👍👍👍👍👍 您的捐助和赞赏，将会是作者howcode更好的维护动力！</code>

